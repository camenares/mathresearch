/*
 * This class extracts the necessary Chess endgame data from http://kirill-kryukov.com/chess/tablebases-online/. It keeps a running 
 * tally of the amount that White has won, the amount that black has won, and the draw count.
 * 
 */
package Analysis;

import static Extraction.DataExtraction.collectData;
import static Extraction.DataExtraction.collectDataWeighted;
import static Extraction.DataExtraction.presentData;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
public class DataAnalysis {   
    
    /**
     * This method creates the URL necessary to then read
     * @return Returns an ArrayList of size 295, that has each URL
     * @throws FileNotFoundException The file to read piece combinations (knnkn)
     * @throws IOException 
     */
    public static ArrayList<String> makeIt() throws FileNotFoundException, IOException
    {
        ArrayList<String> urlContainer = new ArrayList(); //Holds the URLs
        int counter = 1;
        String currentURL = "";
        FileInputStream fis = new FileInputStream("C:\\Users\\Christopher\\Desktop\\Stony Brook\\Spring 2015\\Math Research\\Thesis\\PieceCombinations.txt");
	InputStreamReader inStream = new InputStreamReader(fis);
	BufferedReader reader = new BufferedReader(inStream);
        String line; //Each line of the URL
        while((line = reader.readLine()) != null) //For a given URL, this will read it, line by line
        {   //The if statements are if it is 4+2 w/o pawn, 4+2 w/ pawn, 3+3 w/o pawn, 3+3 w/ pawn
            if(counter <= 80){currentURL = "42/"+line+".tbs";}
            if((counter > 80) && (counter <= 175)){currentURL = "42p/"+line+".tbs";}
            if((counter > 175) && (counter <= 230)){currentURL = "33/"+line+".tbs";}
            if(counter > 230){currentURL = "33p/"+line+".tbs";}
            urlContainer.add(currentURL);
         //   System.out.println(currentURL); //Test to Print
            currentURL = ""; //Reset
            counter++;
            //break;
            
        }
        //System.out.println(urlContainer.size()); Print-Check to make sure Array is 295-long
        return urlContainer;
    }
    
    /**
     * This method reads in the entire webpage's data
     * @param txt This is the URL to read from
     * @return It returns all the data, as one single String
     * @throws IOException 
     */
    public static String Reader() throws IOException
    {
        double[] statistics = new double[3]; //0 index is White Wins, 1 is Black Wins, 2 is Draws
        double[] statisticsWt = new double[3];
        ArrayList<String> urlCaps = makeIt();
        String FIRST_URL = "http://kirill-kryukov.com/chess/tablebases-online/tbs.6-men."; //Blank URL to use
        String summary = ""; //This string will contain some summary of the data
        String summaryWeighted = ""; //This string will contain some summary of the weighted data
        for(int i = 0; i < 295; i++ ) //This will loop through for all piece types. We have 80+95+55+65 = 295 sets to look at
        {
        String FULL_URL = (FIRST_URL)+urlCaps.get(i); //This will change the URL to a particular piece combination
       // System.out.println(FULL_URL);
        URL url = new URL(FULL_URL); //This makes that URL change from String to URL
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream())); //Reads in the URL
        String line; //Each line of the URL
        while((line = in.readLine()) != null) //For a given URL, this will read it, line by line
        {
            collectData(line,statistics); //This keeps track of white's wins, black's wins, and stalemate count          
            collectDataWeighted(line,statisticsWt);//This keeps track of white's wins, black's wins, and stalemate count weighted on the moves to wins
        }
        in.close();

        }
        //The following computes the percentages from all the statistics gathered
        double whitePercentWin = statistics[0]/(statistics[0]+statistics[1]+statistics[2]); 
        double stalematePercent = statistics[2]/(statistics[0]+statistics[1]+statistics[2]);
        double blackPercentWin = 1-(whitePercentWin+stalematePercent);
        System.out.println("White Percent: "+presentData(whitePercentWin)+" Stalemate Percent: "+presentData(stalematePercent)+" Black Percent: "+presentData(blackPercentWin));
        
        double whiteWTPercentWin = statisticsWt[0]/(statisticsWt[0]+statisticsWt[1]+statisticsWt[2]);
        double stalemateWTPercent = statisticsWt[2]/(statisticsWt[0]+statisticsWt[1]+statisticsWt[2]);
        double blackWTPercentWin = 1-(whiteWTPercentWin+stalemateWTPercent);
        System.out.println("White Percent WT: "+presentData(whiteWTPercentWin)+" Stalemate Percent: "+presentData(stalemateWTPercent)+" Black Percent: "+presentData(blackWTPercentWin));

        
        summary += "White Won: "+statistics[0]+" games. Black Won: "+statistics[1]+" games. There were: "+statistics[2]+" stalemates";
        return summary;
    }

    
    
    public static void main(String[] args) throws IOException
    {
            System.out.println(Reader());
           
    }
}
