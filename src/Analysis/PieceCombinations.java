/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analysis;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * This class was for extracting from the web-site piece combinations. Only the main method was used, to print information out, and then copy/paste it into a note-pad.
 * The steps were, first copying and pasting from the source code of http://kirill-kryukov.com/chess/tablebases-online/ 
 * Next, copy and paste every line that looked like: tblink('knnnkn','0.78',2,4);
 * Third, have another method, to print out/extract the inside, knnnkn
 * Paste the list of all knnnkn combinations into another notepad, and use that for the more important methods
 * This class is not neccessary, if you have "PieceCombinations.txt" file. 
 * Just use that text file in the data analysis class, at line 33: FileInputStream fis = new FileInputStream(FILE_PATH_HERE)
 *
 * @author Christopher
 */
public class PieceCombinations {

    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        int counter = 1;
        String printIt = "";
        FileInputStream fis = new FileInputStream("C:\\Users\\Christopher\\Desktop\\Stony Brook\\Spring 2015\\Math Research\\Thesis\\PieceCombinations.txt");
	InputStreamReader inStream = new InputStreamReader(fis);
	BufferedReader reader = new BufferedReader(inStream);
        String line; //Each line of the URL
        while((line = reader.readLine()) != null) //For a given URL, this will read it, line by line
        {
            if(counter <= 80){printIt = "42/"+line+".tbs";}
            if((counter > 80) && (counter <= 175)){printIt = "42p/"+line+".tbs";}
            if((counter > 175) && (counter <= 230)){printIt = "33/"+line+".tbs";}
            if(counter > 230){printIt = "33p/"+line+".tbs";}
            System.out.println(printIt);
            printIt = "";
            counter++;
            //break;
            
        }
    }
}
