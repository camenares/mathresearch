/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Extraction;


/**
 * This class deals with producing the desired data from all the information. It extracts the necessary data
 * @author Christopher
 */
public class DataExtraction  {
    
   /**
    * The method collects the data from each line. 
    * @param currentLinePosition This is the current line (example "wtm: Lose in: 3") being read in.
    * @param statistics This is the current amount of statistics being fed in
    * @return It returns the newly updated statistics
    */
    public static double[] collectData(String currentLinePosition, double[] statistics)
    {
       if(currentLinePosition.contains(":") && !(currentLinePosition.contains("Broken"))) //Ensures only useful lines will be read.
       {
        int firstColon = currentLinePosition.indexOf(":");
        int lastColon = currentLinePosition.lastIndexOf(":");
        
        if(currentLinePosition.substring(0, firstColon).equals("wtm")) //Indicates this is result for white to move
        {
            if(currentLinePosition.contains("Lost in")) //Indicates this is a result where, white to move, it loses
            {
                double wL = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());
                statistics[1] += wL;
            }
            else if(currentLinePosition.contains("Mate in")) //Indicates this is a result where, white to move, it wins
            {
                double wW = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());
                statistics[0] += wW;
            }
            else if(currentLinePosition.contains("Draws")) //Indicates this is a result where white to move, draws
            {
                double DRAW = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());
                statistics[2] += DRAW;
            }
            
        }
        if(currentLinePosition.substring(0, firstColon).equals("btm")) //Indicates this is result for black to move
        {
            if(currentLinePosition.contains("Lost in")) //Indicates this is a result where, black to move, it loses
            {
                double bL = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());
                statistics[0] += bL;
            }
            else if(currentLinePosition.contains("Mate in")) //Indicates this is a result where, black to move, it wins
            {
                double bW = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());  
                statistics[1] += bW;
            }
            if(currentLinePosition.contains("Draws")) //Indicates this is a result where black to move, draws
            {
                double DRAW = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());
                statistics[2] += DRAW;
            }
            
        }
       }
        return statistics;
    }
    /**
     * This method is very similar to the one above. However, it weights statistics by the number of moves to either a win or lose. The weight is done logarithmically, (positionAmount)*(ln(1+positionAmount))
     * @param currentLinePosition This is the current line (example "wtm: Lose in: 3") being read in.
     * @param statistics This is the current amount of statistics being fed in. weighted only. 
     * @return It returns the newly updated statistics. weighted only.
     */
   public static double[] collectDataWeighted(String currentLinePosition, double[] statistics)
   {
       if(currentLinePosition.contains(":") && !(currentLinePosition.contains("Broken"))) //Ensures only useful lines will be read.
       {
        int firstColon = currentLinePosition.indexOf(":");
        int lastColon = currentLinePosition.lastIndexOf(":");
        
        if(currentLinePosition.substring(0, firstColon).equals("wtm")) //Indicates this is result for white to move
        {
            if(currentLinePosition.contains("Lost in")) //Indicates this is a result where, white to move, it loses
            {
                int theN = currentLinePosition.indexOf("n"); //Finds the n in "Lost in"
                String moves = currentLinePosition.substring(theN+1,lastColon); //Gets the amount of moves
                int movesToLose =  Integer.parseInt(moves.trim());
                double wL = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());
                wL = wL*Math.log(1+movesToLose); //Weights the amount of positions of lose by the moves to lose
                statistics[1] += wL;
            }
            else if(currentLinePosition.contains("Mate in")) //Indicates this is a result where, white to move, it wins
            {
                int theN = currentLinePosition.indexOf("n");//Finds the n in "Lost in"
                String moves = currentLinePosition.substring(theN+1,lastColon);//Gets the amount of moves
                int movesToMate =  Integer.parseInt(moves.trim());
                //System.out.println(movesToMate);
                double wW = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());
                wW = wW*Math.log(1+movesToMate); //Weights the amount of positions of win by the moves to win
                statistics[0] += wW;
            }
            else if(currentLinePosition.contains("Draws")) //Indicates this is a result where white to move, draws
            {
                double DRAW = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());
                statistics[2] += DRAW;
            }
            
        }
        if(currentLinePosition.substring(0, firstColon).equals("btm")) //Indicates this is result for black to move
        {
            if(currentLinePosition.contains("Lost in")) //Indicates this is a result where, black to move, it loses
            {
                int theN = currentLinePosition.indexOf("n"); //Finds the n in "Lost in"
                String moves = currentLinePosition.substring(theN+1,lastColon); //Gets the amount of moves
                int movesToLose =  Integer.parseInt(moves.trim());
                double bL = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());
                bL = bL*Math.log(1+movesToLose); //Weights the amount of positions of lose by the moves to lose
                statistics[0] += bL;
            }
            else if(currentLinePosition.contains("Mate in")) //Indicates this is a result where, black to move, it wins
            {
                int theN = currentLinePosition.indexOf("n");//Finds the n in "Lost in"
                String moves = currentLinePosition.substring(theN+1,lastColon);//Gets the amount of moves
                int movesToMate =  Integer.parseInt(moves.trim());
                double bW = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());
                bW = bW*Math.log(1+movesToMate); //Weights the amount of positions of win by the moves to win
                statistics[1] += bW;
            }
            if(currentLinePosition.contains("Draws")) //Indicates this is a result where black to move, draws
            {
                double DRAW = Double.parseDouble(currentLinePosition.substring(lastColon+1).trim());
                statistics[2] += DRAW;
            }
            
        }
       }
        return statistics;
   }
    
   /**
    * This simple method just makes a double like this: 0.34453 turn into a String like this: 34.4%
    * @param percent The double to be changed
    * @return A string is returned, of the form: XX.X%
    */
   public static String presentData(double percent)
   {
       percent = percent*1000;
       percent = (int) percent;
       percent = (double) percent;
       percent = percent/10;
       return percent+"%";
   }
}
